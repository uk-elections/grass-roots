<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class DBConnectionTest extends TestCase
{


    /**
     * Check we can connect the database and get a result.
     *
     * @return void
     */
    public function test_database_connection()
    {
        $one = DB::scalar('select 1');
        $this->assertEquals(1, $one);
    }
}
