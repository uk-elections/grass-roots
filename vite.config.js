import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import fs from 'fs';
 
const host = '192.168.55.13';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.js'],
            refresh: true,
        }),
    ],
    server: {
        host,
        hmr: { host },
        https: {
            key: fs.readFileSync(`/app/.docker/privkey.pem`),
            cert: fs.readFileSync(`/app/.docker/fullchain.pem`),
        },
    },
});
