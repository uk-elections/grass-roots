<?php

namespace App\Support;

use Illuminate\Support\Facades\App;
use Spatie\Csp\Directive;
use Spatie\Csp\Policies\Basic;
use Spatie\Csp\Keyword;

class CspPolicy extends Basic
{
    public function configure()
    {
        parent::configure();
        if (App::environment('local')) {
            $this->addDirective(Directive::SCRIPT, '192.168.55.13:5173');
            $this->addDirective(Directive::STYLE, '192.168.55.13:5173');
            // $this->addDirective(Directive::SCRIPT, Keyword::UNSAFE_INLINE);
            // $this->addDirective(Directive::STYLE, Keyword::UNSAFE_INLINE);
        }
    }
}
